import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product } from '../models/Product';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  productsUrl = 'http://localhost:3000/products'
  productsLimit: string = '?_limit=5';

  constructor(private http: HttpClient) { }

  // Get products
  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.productsUrl}${this.productsLimit}`);
  }
  
}
