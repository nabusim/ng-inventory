import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Product } from '../../models/Product';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  productsList: Product[];
  @Output() onProductSelected: EventEmitter<Product>;
  private currentProduct: Product;

  constructor(private productsSercice: ProductsService) {
    this.onProductSelected = new EventEmitter();
  }

  ngOnInit() {
    this.productsSercice.getProducts().subscribe(products => {
      console.log(products);
      this.productsList = products;
    });
  }

  clicked(product: Product): void {
    this.currentProduct = product;
    this.onProductSelected.emit(product);
  }

}
